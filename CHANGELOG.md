# Changelog
All notable changes to this package will be documented in this file

## Unreleased

## 1.1.2 - 2023-09-01
- Bumped dependencies [patch]

## 1.1.1 - 2023-05-28
- Hotfix: fix `slevomat-coding-standard` ruleset [patch]

## 1.1.0 - 2023-05-28
- Bumped PHP requirement to 8.2 and bumped Composer dependencies [minor]
- Added Forgejo test workflows [patch]
- Updated `README` & `LICENSE` [patch]

## 1.0.1 - 2022-08-18
- Added exclusion for `SlevomatCodingStandard.Classes.DisallowLateStaticBindingForConstants` to allow late static binding for constants [patch]

## 1.0.0 - 2022-08-20
- Initial release [major]

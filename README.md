# Crawl Coding Standard

[![Latest Version on Packagist](https://img.shields.io/packagist/v/crawl/coding-standard.svg?style=flat-square)](https://packagist.org/packages/crawl/coding-standard)
[![Total Downloads](https://img.shields.io/packagist/dt/crawl/coding-standard.svg?style=flat-square)](https://packagist.org/packages/crawl/coding-standard)

This package provides rule sets for the Crawl Coding Standard.
It requires `squizlabs/php_codesniffer` and some extensions (see [composer.json](composer.json)).

## Installation

You can install the package via composer (you should only need it for development):

```bash
composer require crawl/coding-standard --dev
```

## Usage
Depending on your project, copy `phpcs-laravel.xml` or `phpcs-package.xml` from `config/` to your project.

> **Note:** don't forget to update `Package\Namespace` when using `phpcs-package.xml`.

You can then run PHP Code Sniffer/Beautifier:
```bash
$ vendor/bin/phpcs --standard=./path/to/copied/phpcs.xml
$ vendor/bin/phpcbf --standard=./path/to/copied/phpcs.xml
```

For easier usage, add the following to `scripts` in your project's `composer.json`:
```json
    ...
    "scripts": {
        ...
        "style": "vendor/bin/phpcs --standard=./path/to/copied/phpcs.xml",
        "fix-style": "vendor/bin/phpcbf --standard=./path/to/copied/phpcs.xml",
    }.
    ...
```
You can then use `composer style` to run the sniffer, and `composer fix-style` to run the beautifier.

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email security@gkcld.net instead of using the issue tracker.

## Authors
-   [Gertjan Krol](https://git.gertjankrol.nl/gertjan)

## License

The MIT License (MIT). Please see [LICENSE](LICENSE.md) for more information.
